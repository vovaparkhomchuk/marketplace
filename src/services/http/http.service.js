import axios from 'axios'

class HttpService {
  async get(url) {
    const response = await axios.get(url)
    if (response && 'data' in response) {
      return response.data
    }
    throw new Error('Get request error.')
  }

  async post(url, data) {
    const response = await axios.post(url, data)
    if (response && 'data' in response) {
      return response.data
    }
    throw new Error('Get request error.')
  }

  async postWithHeader(url, data, token) {
    const response = await axios.post(url, data, {
      headers: {
        Authorization: `Bearer ${token}`
      },
    })
    if (response && 'data' in response) {
      return response.data
    }
    throw new Error('Get request error.')
  }

  async delete(url, token) {
    const response = await axios.delete(url, {
      headers: {
        Authorization: `Bearer ${token}`
      },
    })
    if (response && 'data' in response) {
      return response.data
    }
    throw new Error('Get request error.')
  }
}

const httpService = new HttpService()
export default httpService
