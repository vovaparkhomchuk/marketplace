import httpService from "../http/http.service"

class ProductsService {
  async getProducts() {
    try {
      return await httpService.get('https://bsa-marketplace.azurewebsites.net/api/Products')
    }
    catch (e) {
      console.error({e})
    }
  }

  async getProduct(id) {
    try {
      return await httpService.get(`https://bsa-marketplace.azurewebsites.net/api/Products/${id}`)
    }
    catch (e) {
      console.error({e})
    }
  }

  async addProduct(data, token) {
    try {
      return await httpService.postWithHeader(`https://bsa-marketplace.azurewebsites.net/api/Products`, data, token)
    }
    catch (e) {
      console.error({e})
    }
  }

  async deleteProduct(id , token) {
    try {
      return await httpService.delete(`https://bsa-marketplace.azurewebsites.net/api/Products/${id}`, token)
    }
    catch (e) {
      console.error({e})
    }
  }
}

const productsService = new ProductsService()
export default productsService
