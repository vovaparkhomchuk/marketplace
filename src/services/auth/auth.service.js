import httpService from "../http/http.service"
import AsyncStorage from '@react-native-async-storage/async-storage'
import {STORAGE} from "../../common/constants/storage";


class AuthService {
  async login(data) {
    try {
      return await httpService.post('https://bsa-marketplace.azurewebsites.net/api/Auth/login', data)
    }
    catch (e) {
      console.error({e})
    }
  }

  async signUp(data) {
    try {
      return await httpService.post('https://bsa-marketplace.azurewebsites.net/api/Auth/register', data)
    }
    catch (e) {
      console.error({e})
    }
  }

  async saveUserInStorage(data) {
    try {
      const jsonData = JSON.stringify(data);
      await AsyncStorage.setItem(STORAGE.user, jsonData)
    } catch (e) {
      console.error({e})
    }
  }

  async checkUserInStorage() {
    try {
      const value = await AsyncStorage.getItem(STORAGE.user)
      if (value) return JSON.parse(value);
      return null;
    } catch(e) {
      console.error({e})
    }
  }
}

const authService = new AuthService()
export default authService
