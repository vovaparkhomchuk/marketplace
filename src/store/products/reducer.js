import {createReducer, isAnyOf} from '@reduxjs/toolkit';
import {increment, loadProducts, loadOneProduct, addProduct, deleteProduct} from "./actions";

const initialState = {
  productList: [],
  counter: 0,
  status: "null",
  fullList: {}
};

const reducer = createReducer(initialState, builder => {
  builder
    .addCase(increment, (state, action) => {
      state.counter += 1
    })
    .addCase(loadProducts.pending, (state) => {
      state.status = 'pending'
    })
    .addCase(addProduct.fulfilled, (state, action) => {
      const {product} = action.payload
      // state.productList.push(product)
    })
    .addCase(deleteProduct.fulfilled, (state, action) => {
      const {id} = action.payload
      state.productList = state.productList.filter(product => product.id !== id)
    })
    .addCase(loadProducts.fulfilled, (state, action) => {
      const {products} = action.payload
      state.productList = products
      state.status = 'fulfilled'
    })
    .addCase(loadOneProduct.fulfilled, (state, action) => {
      const {product, id} = action.payload
      state.fullList[id] = product
    })
});

export {reducer};
