import { createAction } from '@reduxjs/toolkit'
import {createAsyncThunk} from "@reduxjs/toolkit/src/createAsyncThunk";
import {ACTION_TYPES} from "../../common/constants/actionsTypes";
import productsService from "../../services/products/products.service";

const increment = createAction('counter/increment')

const loadProducts = createAsyncThunk(
  ACTION_TYPES.setAllProducts,
  async () => {
    const products = await productsService.getProducts();
    return {products};
  })

const loadOneProduct = createAsyncThunk(
  ACTION_TYPES.setOneProduct,
  async (id) => {
    const product = await productsService.getProduct(id);
    return {product, id};
  })

const addProduct = createAsyncThunk(
  ACTION_TYPES.addProduct,
  async (params) => {
    const {data, token} = params;
    const product = await productsService.addProduct(data, token);
    return {product};
  })

const deleteProduct = createAsyncThunk(
  ACTION_TYPES.deleteProduct,
  async (params) => {
    const {id, token} = params;
    await productsService.deleteProduct(id, token);
    return {id};
  })

export {increment, loadProducts, loadOneProduct, addProduct, deleteProduct}
