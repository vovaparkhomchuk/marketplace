import { configureStore } from '@reduxjs/toolkit'
import {productsReducer, authReducer} from "./rootReducer"

export const store = configureStore({
  reducer: {
    products: productsReducer,
    user: authReducer
  },
})
