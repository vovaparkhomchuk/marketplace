import {createAsyncThunk} from "@reduxjs/toolkit/src/createAsyncThunk";
import {ACTION_TYPES} from "../../common/constants/actionsTypes";
import authService from "../../services/auth/auth.service";
import {createAction} from "@reduxjs/toolkit";

const saveUser = createAction('user/save')
const setStorageChecked = createAction('user/checked')

const signIn = createAsyncThunk(
  ACTION_TYPES.signIn,
  async (data) => {
    const user = await authService.login(data);
    if (user) {
      await authService.saveUserInStorage(user)
      return {user};
    }
  })

const signUp = createAsyncThunk(
  ACTION_TYPES.signUp,
  async (data) => {
    const user = await authService.signUp(data);
    if (user)
      return {user};
  })


export {signIn,signUp, saveUser, setStorageChecked}
