import {createReducer, isAnyOf} from '@reduxjs/toolkit';
import {signIn, signUp, saveUser, setStorageChecked, addProduct} from "./actions";

const initialState = {
  user: {},
  checkedStorage: false,
};

const reducer = createReducer(initialState, builder => {
  builder
    .addCase(setStorageChecked, (state) => {
      state.checkedStorage = true
    })
    .addCase(saveUser, (state, action) => {
      state.user = {...action.payload}
      state.checkedStorage = true
    })
    .addCase(signIn.fulfilled, (state, action) => {
      const {user} = action.payload
      state.user = {...user}
      state.checkedStorage = true
    })
    .addCase(signUp.fulfilled, (state, action) => {
      alert('You have signed up, now sign in please')
    })
});

export {reducer};
