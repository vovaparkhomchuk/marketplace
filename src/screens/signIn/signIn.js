import React, {useEffect, useState} from "react";
import {View, Text, TextInput, StyleSheet, TouchableOpacity} from "react-native";
import {SCREEN} from "../../common/constants/screens";
import {useDispatch, useSelector} from "react-redux";
import {signIn} from "../../store/auth/actions";

const SignIn = ({navigation}) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const user = useSelector(state => state)
  const dispatch = useDispatch()

  const handleNavigate = () => {
    navigation.navigate(SCREEN.signUp)
  }

  const handleSignIn = async () => {
    if (email && password)
      dispatch(signIn({email, password}))
  }

  return (
    <View style={styles.container}>
      <View>
        <TextInput
          style={styles.input}
          value={email}
          placeholder={'Email'}
          onChangeText={setEmail}/>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          value={password}
          placeholder={'Password'}
          onChangeText={setPassword}/>
        <TouchableOpacity
          onPress={handleSignIn}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Sign in</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleNavigate}
          style={styles.button2}
        >
          <Text style={styles.buttonText2}>or sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingHorizontal: 12,
    justifyContent: 'space-around',
    flex: 1
  },
  input: {
    borderColor: 'black',
    borderBottomWidth: 1,
    paddingHorizontal: 12,
    marginBottom: 12,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    borderRadius: 12
  },
  button2: {
    marginTop: 12,
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    borderRadius: 12
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  buttonText2: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold'
  }
})

export default SignIn;
