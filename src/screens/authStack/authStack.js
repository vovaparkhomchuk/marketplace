import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SignIn from "../signIn/signIn";
import SignUp from "../signUp/signUp";
import {SCREEN} from "../../common/constants/screens";

const Stack = createNativeStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name={SCREEN.signIn} component={SignIn}/>
      <Stack.Screen name={SCREEN.signUp} component={SignUp}/>
    </Stack.Navigator>
  );
}

export default AuthStack;
