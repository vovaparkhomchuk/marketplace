import * as React from 'react';
import {Button} from "react-native";
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ProductList from "../productList/productList";
import Product from "../product/product";
import AddProduct from "../addProduct/addProduct";
import {SCREEN} from "../../common/constants/screens";

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={SCREEN.productList}
        component={ProductList}
        options={({ navigation, route }) => ({
          headerRight: () => (
            <Button
              onPress={() => navigation.navigate(SCREEN.addProduct)}
              title="Add"
              color="black"
            />
          ),
        })}/>
      <Stack.Screen name={SCREEN.product} component={Product}/>
      <Stack.Screen name={SCREEN.addProduct} component={AddProduct}/>
    </Stack.Navigator>
  );
}

export default MainStack;
