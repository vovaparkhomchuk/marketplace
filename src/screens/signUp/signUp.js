import React, {useState} from "react";
import {View, Text, TextInput, StyleSheet, TouchableOpacity} from "react-native";
import {signUp} from "../../store/auth/actions";
import {useDispatch} from "react-redux";

const SignUp = ({navigation}) => {
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()

  const handleNavigate = () => {
    navigation.goBack()
  }

  const handleSignUp = async () => {
    if (email && password)
      dispatch(signUp({email, name, phoneNumber, password}))
  }

  return (
    <View style={styles.container}>
      <View>
        <TextInput
          style={styles.input}
          value={email}
          placeholder={'Email'}
          onChangeText={setEmail}/>
        <TextInput
          style={styles.input}
          value={name}
          placeholder={'Name'}
          onChangeText={setName}/>
        <TextInput
          style={styles.input}
          value={phoneNumber}
          placeholder={'Phone number'}
          onChangeText={setPhoneNumber}/>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          value={password}
          placeholder={'Password'}
          onChangeText={setPassword}/>
        <TouchableOpacity onPress={handleSignUp} style={styles.button}>
          <Text style={styles.buttonText}>Sign up</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleNavigate}
          style={styles.button2}>
          <Text style={styles.buttonText2}>or sign in</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingHorizontal: 12,
    justifyContent: 'space-around',
    flex: 1
  },
  input: {
    borderColor: 'black',
    borderBottomWidth: 1,
    paddingHorizontal: 12,
    marginBottom: 12,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    borderRadius: 12
  },
  button2: {
    marginTop: 12,
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    borderRadius: 12
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  buttonText2: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold'
  }
})

export default SignUp
