import React, {useEffect, useState, useMemo} from 'react'
import {FlatList, StyleSheet, TextInput, View} from 'react-native'
import {useDispatch, useSelector} from "react-redux";
import {loadProducts} from "../../store/products/actions";
import {ProductItem} from '../../components/components';

const ProductList = ({navigation}) => {
  const [search, setSearch] = useState('')
  const dispatch = useDispatch()
  const {status, productList} = useSelector(state => state.products)
  const filteredProducts = useMemo(() => productList.filter(product => product.title.toLowerCase().includes(search.toLowerCase()), [search]))


  const handlePullToUpdate = () => {
    dispatch(loadProducts())
  }

  useEffect(() => {
    dispatch(loadProducts())
  }, [])

  return (
    <>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          value={search}
          placeholder={'Search'}
          onChangeText={text => setSearch(text)} />
      </View>
      <FlatList
        data={search === '' ? productList : filteredProducts}
        onRefresh={handlePullToUpdate}
        refreshing={status === 'pending'}
        renderItem={({item}) => <ProductItem item={item} navigation={navigation}/>}/>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingHorizontal: 12
  },
  input: {
    borderColor: 'black',
    borderRadius: 12,
    borderWidth: 1,
    paddingHorizontal: 12
  }
})


export default ProductList;
