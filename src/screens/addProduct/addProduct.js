import React, {useState} from "react";
import {View, Text, TextInput, StyleSheet, TouchableOpacity} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {addProduct, loadProducts} from "../../store/products/actions";

const AddProduct = ({navigation}) => {
  const [title, setTitle] = useState('')
  const [price, setPrice] = useState('')
  const [description, setDescription] = useState('')
  const dispatch = useDispatch()
  const {user} = useSelector(state => state.user)

  const handleAddNewProduct = () => {
    if (title && price && description) {
      const product = {title, price, description}
      dispatch(addProduct({data: product, token: user.token}))
      dispatch(loadProducts())
      navigation.goBack()
    }
  }

  return (
    <View style={styles.container}>
      <View>
        <TextInput
          style={styles.input}
          value={title}
          placeholder={'Title'}
          onChangeText={setTitle}/>
        <TextInput
          style={styles.input}
          value={price}
          placeholder={'Price'}
          onChangeText={setPrice}/>
        <TextInput
          style={styles.input}
          value={description}
          placeholder={'Description'}
          onChangeText={setDescription}/>
        <TouchableOpacity
          onPress={handleAddNewProduct}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Add new product</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingHorizontal: 12,
    justifyContent: 'space-around',
    flex: 1
  },
  input: {
    borderColor: 'black',
    borderBottomWidth: 1,
    paddingHorizontal: 12,
    marginBottom: 12,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    borderRadius: 12
  },
  button2: {
    marginTop: 12,
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    borderRadius: 12
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  buttonText2: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold'
  }
})

export default AddProduct
