import React, {useEffect} from 'react'
import {View, Text, ScrollView, Image, StyleSheet, Dimensions, Linking, TouchableOpacity} from 'react-native'
import {useDispatch, useSelector} from "react-redux";
import {deleteProduct, loadOneProduct} from "../../store/products/actions";

const Product = ({navigation, route}) => {
  const {id} = route.params;
  const dispatch = useDispatch()
  const {fullList} = useSelector(state => state.products)
  const {user} = useSelector(state => state.user)

  const {width} = Dimensions.get('window');

  const handleLoadOneProduct = () => {
    if (!(id in fullList))
      dispatch(loadOneProduct(id))
  }

  const handlePressPhoneNumber = async (number) => {
    await Linking.openURL(`tel:${number}`)
  }

  const handleDelete = () => {
    dispatch(deleteProduct({id, token: user.token}))
    navigation.goBack()
  }

  useEffect(handleLoadOneProduct, [])


  if (!fullList[id]) return (<Text>Loading...</Text>)

  return (
    <View>
      <ScrollView>
        {fullList[id].seller.email === user.email &&
          <TouchableOpacity style={styles.delete} onPress={handleDelete}>
            <Text style={styles.deleteText}>Delete</Text>
          </TouchableOpacity>
        }
        <ScrollView style={styles.image} horizontal={true} pagingEnabled={true}>
          {fullList[id].pictures.length > 0 && fullList[id].pictures.map(picture => (
            <Image
              key={picture.id}
              style={[styles.image, {width}]}
              source={{
                uri: picture.url,
              }}
            />
          ))}
        </ScrollView>
        <View style={styles.info}>
          <View style={styles.horizontal}>
            <Text style={styles.mainText}>{fullList[id].title}</Text>
            <Text style={styles.mainText}>${fullList[id].price}</Text>
          </View>
          <Text style={styles.description}>{fullList[id].description}</Text>
          <View style={styles.horizontal}>
            <Image
              style={{width: 80, height: 80}}
              source={{
                uri: fullList[id].seller.avatar,
              }}
            />
            <View style={[{width: width - 120}]}>
              <Text>{fullList[id].seller.name}</Text>
              <Text>{fullList[id].seller.email}</Text>
              <TouchableOpacity
                onPress={() => handlePressPhoneNumber(fullList[id].seller.phoneNumber)}
              >
                <Text style={styles.phoneNumber}>{fullList[id].seller.phoneNumber}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  image: {
    height: 300
  },
  info: {
    flex: 1,
    paddingHorizontal: 12,
    marginVertical: 12
  },
  description: {
    marginBottom: 12
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 12
  },
  mainText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  phoneNumber: {
    color: '#0074cc'
  },
  delete: {
    width: '100%',
    backgroundColor: '#c74646',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center'
  },
  deleteText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    borderRadius: 12,
    marginVertical: 12
  }
})

export default Product;
