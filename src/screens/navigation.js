import * as React from 'react';
import MainStack from './mainStack/mainStack';
import AuthStack from "./authStack/authStack";
import {useDispatch, useSelector} from "react-redux";
import authService from "../services/auth/auth.service";
import {useEffect} from "react";
import {saveUser, setStorageChecked} from "../store/auth/actions";

const Navigation = () => {
  const {user, checkedStorage} = useSelector(state => state.user)
  const dispatch = useDispatch()

  useEffect(() => {
    authService.checkUserInStorage().then(user => {
      if (user) dispatch(saveUser(user))
      dispatch(setStorageChecked())
    })
  }, [])

  if (checkedStorage)
    return (
      <>
        {user && user.token ? <MainStack/> : <AuthStack/>}
      </>

    );
  else return (<></>)
}

export default Navigation;
