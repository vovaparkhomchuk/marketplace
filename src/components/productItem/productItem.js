import React from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {SCREEN} from "../../common/constants/screens";
import {useDispatch, useSelector} from "react-redux";

const ProductItem = ({item, navigation}) => {
  const {id, title, description, price, picture} = item

  const navigateToProduct = () => {
    navigation.navigate(SCREEN.product, {id})
  }

  return (
    <TouchableOpacity
      onPress={navigateToProduct}
      style={styles.item}>
      <Image
        style={styles.image}
        source={{
          uri: picture,
        }}
      />
      <View style={styles.itemInfo}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.description}>{description.slice(0, 60)+"..."}</Text>
        <Text style={styles.price}>${price}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    paddingHorizontal: 12,
    marginVertical: 12
  },
  itemInfo: {
    marginHorizontal: 12,
    flex: 1
  },
  image: {
    width: 100,
    height: 100,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 6
  },
  description: {
    fontSize: 12,
    marginBottom: 6
  },
  price: {
    fontSize: 14,
    fontWeight: 'bold'
  },
})

export default ProductItem;
