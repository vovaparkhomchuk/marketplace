import React, {useState} from "react";
import {View, TextInput, StyleSheet} from "react-native";

const Search = () => {
  const [search, setSearch] = useState('')

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        value={search}
        placeholder={'Search'}
        onChange={text => setSearch(text)} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingHorizontal: 12
  },
  input: {
    borderColor: 'black',
    borderRadius: 12,
    borderWidth: 1,
    paddingHorizontal: 12
  }
})

export default Search;
