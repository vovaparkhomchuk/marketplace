export const ACTION_TYPES = {
  setAllProducts: 'setAllProducts',
  setOneProduct: 'setOneProduct',
  addProduct: 'addProduct',
  deleteProduct: 'deleteProduct',
  signIn: 'signIn',
  signUp: 'signUp',
}
